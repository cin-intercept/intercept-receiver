package com.cin.intercept.receiver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InterceptReceiverApplication {

	public static void main(String[] args) {
		SpringApplication.run(InterceptReceiverApplication.class, args);
	}

}
