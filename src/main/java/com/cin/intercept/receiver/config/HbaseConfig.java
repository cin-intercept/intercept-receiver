package com.cin.intercept.receiver.config;

import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
public class HbaseConfig {

    @Value("${data.hbase.zkQuorum}")
    private String zkQuorum;

    @Value("${data.hbase.clientPort}")
    private String clientPort;

    @Value("${data.hbase.zkBasePath}")
    private String zkBasePath;

    @Value("${data.hbase.rootDir}")
    private String rootDir;

    @Bean
    public org.apache.hadoop.conf.Configuration configuration() {
        org.apache.hadoop.conf.Configuration configuration = HBaseConfiguration.create();
        configuration.set("hbase.zookeeper.quorum", zkQuorum);
        configuration.set("hbase.zookeeper.property.clientPort", clientPort);
        configuration.set("hbase.rootdir", rootDir);
        configuration.set("zookeeper.znode.parent", zkBasePath);
        return configuration;
    }

    @Bean
    public Connection hbaseConnection() throws IOException {
        HBaseAdmin.available(configuration());
        return ConnectionFactory.createConnection(configuration());
    }
}
