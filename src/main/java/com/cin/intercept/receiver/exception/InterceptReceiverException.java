package com.cin.intercept.receiver.exception;

public class InterceptReceiverException extends Exception {

    private String description;

    public InterceptReceiverException(String description) {
        super();
        this.description = description;
    }
}
