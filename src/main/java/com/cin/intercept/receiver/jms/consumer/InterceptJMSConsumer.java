package com.cin.intercept.receiver.jms.consumer;

import com.cin.intercept.receiver.exception.InterceptReceiverException;
import com.cin.intercept.receiver.service.RegistroOcorrenciaTransitoService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class InterceptJMSConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(InterceptJMSConsumer.class);

    @Autowired
    private RegistroOcorrenciaTransitoService registroOcorrenciaTransitoService;

    @JmsListener(destination = "${active-requeue-intercept-receiver-name}")
    public void messageConsumer(String message) throws IOException, InterceptReceiverException {
        LOGGER.info("InterceptJMSConsumer.messageConsumer date {}, body = {}", LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss")), message);
        JSONArray jsonArray = new JSONArray();
        try {
            jsonArray.put(new JSONObject(message));
            printMessageProccessor(jsonArray.length());
        } catch (Exception e) {
            try {
                jsonArray = new JSONArray(message);
                printMessageProccessor(jsonArray.length());
            } catch (Exception ex) {
                LOGGER.error("InterceptJMSConsumer.messageConsumer - Error on message processing: ", ex);
                throw new InterceptReceiverException(ex.getMessage());
            }
        }
        registroOcorrenciaTransitoService.inserirOcorrenciaTransito(jsonArray);
    }

    private void printMessageProccessor(int length) {
        LOGGER.info("InterceptJMSConsumer.messageConsumer = Processando {} Registros de Passagem", length);
    }
}
