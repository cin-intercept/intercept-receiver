package com.cin.intercept.receiver.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Table(name = "intercept_registro_passagem")
@Entity
public class RegistroPassagem {
    @Id
    private Long id;

    @Column(name = "data_insercao")
    private String dataInsercao;

    @Column(name = "id_ocorrencia")
    private String idOcorrencia;

    @Column(name = "id_local_antena")
    private String idLocalAntena;

    @Column(name = "placa")
    private String placa;

    @Column(name = "data_passagem")
    private String data;

    @Column(name = "imagem", columnDefinition = "text")
    private String imagem;
}
