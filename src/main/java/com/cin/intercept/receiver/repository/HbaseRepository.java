package com.cin.intercept.receiver.repository;

import com.cin.intercept.receiver.model.RegistroPassagem;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.IOException;

@Repository
public class HbaseRepository {

    @Value("${data.hbase.tb.registro.passagem}")
    private String tableName;

    @Value("${data.hbase.cf.registro.passagem}")
    private String cf;

    private Connection connection;

    @Autowired
    public HbaseRepository(Connection connection) {
        this.connection = connection;
    }

    public void putRegistroPassagem(String placa, RegistroPassagem passagem) throws IOException {
        Table table = connection.getTable(TableName.valueOf(Bytes.toBytes(tableName)));
        Put put = new Put(Bytes.toBytes(placa + "_" + passagem.getId().toString()));
        put.addColumn(Bytes.toBytes(cf), Bytes.toBytes("data_insercao"), Bytes.toBytes(passagem.getDataInsercao()));
        put.addColumn(Bytes.toBytes(cf), Bytes.toBytes("id_ocorrencia"), Bytes.toBytes(passagem.getIdOcorrencia()));
        put.addColumn(Bytes.toBytes(cf), Bytes.toBytes("id_local_antena"), Bytes.toBytes(passagem.getIdLocalAntena()));
        put.addColumn(Bytes.toBytes(cf), Bytes.toBytes("placa"), Bytes.toBytes(passagem.getPlaca()));
        put.addColumn(Bytes.toBytes(cf), Bytes.toBytes("data_passagem"), Bytes.toBytes(passagem.getData()));
        put.addColumn(Bytes.toBytes(cf), Bytes.toBytes("imagem"), Bytes.toBytes(passagem.getImagem()));
		table.put(put);
    }
}
