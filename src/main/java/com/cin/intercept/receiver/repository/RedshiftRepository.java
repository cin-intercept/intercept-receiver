package com.cin.intercept.receiver.repository;

import com.cin.intercept.receiver.model.RegistroPassagem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RedshiftRepository extends JpaRepository<RegistroPassagem, Integer> {

}
