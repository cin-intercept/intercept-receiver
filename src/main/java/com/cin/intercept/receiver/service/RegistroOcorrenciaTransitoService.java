package com.cin.intercept.receiver.service;

import com.amazonaws.services.sqs.AmazonSQSAsync;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.cin.intercept.receiver.jms.consumer.InterceptJMSConsumer;
import com.cin.intercept.receiver.model.RegistroPassagem;
import com.cin.intercept.receiver.repository.HbaseRepository;
import com.cin.intercept.receiver.repository.RedshiftRepository;
import com.cin.intercept.receiver.utils.BuilderUtils;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class RegistroOcorrenciaTransitoService {

    private static final Logger LOGGER = LoggerFactory.getLogger(InterceptJMSConsumer.class);

    @Value("${aws.sqs-requeue-intercept-producer-name}")
    private String queueUrl;

    @Autowired
    AmazonSQSAsync sqsAsync;

    @Autowired
    private HbaseRepository hbaseRepository;

    @Autowired
    private RedshiftRepository redshiftRepository;

    public void inserirOcorrenciaTransito(JSONArray jsonArray) throws IOException {

        for (int i = 0; i < jsonArray.length(); i++) {
            RegistroPassagem passagem = BuilderUtils.registroPassagemBuilder(jsonArray.getJSONObject(i));
            LOGGER.info("RegistroOcorrenciaTransitoService.inserirOcorrenciaTransito passagem = {}", passagem.toString());
            hbaseRepository.putRegistroPassagem(passagem.getPlaca(), passagem);
            redshiftRepository.save(passagem);
        }

        sendPassagemUpdateNotificacao(jsonArray);
    }

    public void sendPassagemUpdateNotificacao(JSONArray jsonArray) {

        LOGGER.info("RegistroOcorrenciaTransitoService.sendPassagemUpdateNotificacao = Sending {} Registros de Passagem", jsonArray.length());

        SendMessageRequest request = new SendMessageRequest()
                .withQueueUrl(queueUrl)
                .withMessageBody(jsonArray.toString())
                .withDelaySeconds(5);

        sqsAsync.sendMessage(request);
    }
}
