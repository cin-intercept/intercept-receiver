package com.cin.intercept.receiver.utils;

import com.cin.intercept.receiver.model.RegistroPassagem;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class BuilderUtils {

    public static RegistroPassagem registroPassagemBuilder(@NotNull JSONObject json) {

        LocalDateTime dtHoraPassagem = LocalDateTime.now();
        String dataFormat = dtHoraPassagem.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));

        return RegistroPassagem.builder()
                .id(dtHoraPassagem.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli())
                .dataInsercao(dataFormat)
                .idOcorrencia(json.getString("id_ocorrencia"))
                .idLocalAntena(json.getString("id_local_antena"))
                .placa(json.getString("placa"))
                .data(json.getString("data"))
                .imagem(json.getString("imagem"))
                .build();
    }


}
